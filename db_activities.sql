-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 29, 2016 at 12:52 AM
-- Server version: 5.7.12-0ubuntu1.1
-- PHP Version: 7.0.4-7ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_activities`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_activities`
--

CREATE TABLE `tb_activities` (
  `id` int(11) NOT NULL,
  `categories_id` int(11) DEFAULT NULL,
  `activities_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activities_date` date DEFAULT NULL,
  `activities_detail` longtext COLLATE utf8_unicode_ci,
  `activities_year` int(4) DEFAULT NULL,
  `activities_term` int(2) DEFAULT NULL,
  `activities_for` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activities_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_activities`
--

INSERT INTO `tb_activities` (`id`, `categories_id`, `activities_name`, `activities_date`, `activities_detail`, `activities_year`, `activities_term`, `activities_for`, `activities_by`, `created_at`, `updated_at`) VALUES
(1, 4, 'กิจกรรมวันไหว้ครู', '2016-06-17', '<p>กิจกรรมไหว้ครู</p>\r\n', 2559, 1, '9', 1, '2016-06-17 09:22:05', '2016-06-19 06:50:01'),
(2, 4, 'กิจกรรมรับน้องใหม่ ประจำปีการศึกษา 2559', '2016-06-17', '<p>รับน้องใหม่</p>\r\n', 2559, 1, '123', 1, '2016-06-17 09:23:55', '2016-06-17 09:23:55'),
(3, 5, 'กิจกรรมวันเข้าพรรษา', '2016-06-20', '', 2559, 1, '1', 1, '2016-06-17 09:42:51', '2016-06-17 09:42:51'),
(4, 4, 'ทดสอบเพื่อลบ', '2016-06-23', '', 2559, 1, '135', 1, '2016-06-23 16:15:53', '2016-06-23 16:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_activities_status`
--

CREATE TABLE `tb_activities_status` (
  `id` int(11) NOT NULL,
  `activities_id` int(11) DEFAULT NULL,
  `student_id` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_activities_status`
--

INSERT INTO `tb_activities_status` (`id`, `activities_id`, `student_id`) VALUES
(1, 1, '47122420302');

-- --------------------------------------------------------

--
-- Table structure for table `tb_categories`
--

CREATE TABLE `tb_categories` (
  `id` int(11) NOT NULL,
  `categories_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_categories`
--

INSERT INTO `tb_categories` (`id`, `categories_name`, `created_at`, `updated_at`) VALUES
(4, 'กิจกรรมบังคับ', '2016-06-11 17:34:29', '2016-06-11 17:34:29'),
(5, 'กิจกรรมเกี่ยวกับพระพุทธศาสนาและวัฒนธรรม', '2016-06-11 17:46:14', '2016-06-11 17:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_depart`
--

CREATE TABLE `tb_depart` (
  `id` int(11) NOT NULL,
  `depart_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculties_id` int(6) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_depart`
--

INSERT INTO `tb_depart` (`id`, `depart_name`, `faculties_id`, `created_at`, `updated_at`) VALUES
(1, 'สาขาวิทยาการคอมพิวเตอร์', 1, '2016-06-14 07:27:26', '2016-06-14 07:27:26'),
(2, 'สาขาเทคโนโลยีสารสนเทศ', 1, '2016-06-14 07:30:43', '2016-06-14 07:30:43');

-- --------------------------------------------------------

--
-- Table structure for table `tb_faculties`
--

CREATE TABLE `tb_faculties` (
  `id` int(11) NOT NULL,
  `faculties_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_faculties`
--

INSERT INTO `tb_faculties` (`id`, `faculties_name`, `created_at`, `updated_at`) VALUES
(1, 'คณะวิทยาศาสตร์และเทคโนโลยี', '2016-06-11 17:58:40', '2016-06-11 17:58:40'),
(2, 'คณะวิทยาการจัดการ', '2016-06-14 07:52:56', '2016-06-14 07:52:56'),
(3, 'คณะคุรุศาสตร์', '2016-06-14 07:53:18', '2016-06-14 07:53:18'),
(4, 'คณะมนุษศาสตร์และสังคมศาสตร์', '2016-06-14 07:53:52', '2016-06-14 07:53:52');

-- --------------------------------------------------------

--
-- Table structure for table `tb_student`
--

CREATE TABLE `tb_student` (
  `id` int(11) NOT NULL,
  `student_id` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` int(11) NOT NULL,
  `student_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `student_lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `faculties_id` int(11) NOT NULL,
  `depart_id` int(11) NOT NULL,
  `student_year` int(11) NOT NULL,
  `student_class` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_student`
--

INSERT INTO `tb_student` (`id`, `student_id`, `title`, `student_name`, `student_lastname`, `gender`, `faculties_id`, `depart_id`, `student_year`, `student_class`, `created_at`, `updated_at`) VALUES
(1, '47122420302', 1, 'สนธยา', 'อุปถัมภ์', 1, 1, 1, 1, 3, '2016-06-14 11:09:57', '2016-06-14 11:09:57');

-- --------------------------------------------------------

--
-- Table structure for table `tb_title`
--

CREATE TABLE `tb_title` (
  `id` int(11) NOT NULL,
  `title_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_title`
--

INSERT INTO `tb_title` (`id`, `title_name`, `created_at`, `updated_at`) VALUES
(1, 'นาย', '2016-06-18 08:15:34', '2016-06-18 08:15:34'),
(2, 'นางสาว', '2016-06-18 08:15:41', '2016-06-18 08:15:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_name`, `user_status`, `user_type`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'admin', '$2y$10$Zwr1eYuh.Fq8CklBznXW7uCPVTEhPphOa/cFH0lCC.0UBa77nfdki', 'Administrator', '1', '1', '0000-00-00 00:00:00', '2016-06-28 10:11:49', 'ej9NzVfPyujXzWoa43rpsdE5atM7hyDJmgpXhGEL1KhIk7XChYu0rLal98Z6'),
(2, '47122420302', '$2y$10$t3dtd.eGgpB1PHZ9.qiX5e0cfS1CPVZDjp6aYf8KoZyq3YMZJ0.C6', NULL, '1', '2', '2016-06-14 11:09:57', '2016-06-28 10:11:31', 'w48oHaeOqWPCrXRtLrN5fKFzlQjyWQN6LcyMq3lwBO9yiTfbtKM4iT4UASYo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_activities`
--
ALTER TABLE `tb_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_activities_status`
--
ALTER TABLE `tb_activities_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_categories`
--
ALTER TABLE `tb_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_depart`
--
ALTER TABLE `tb_depart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_faculties`
--
ALTER TABLE `tb_faculties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_student`
--
ALTER TABLE `tb_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_title`
--
ALTER TABLE `tb_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_activities`
--
ALTER TABLE `tb_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_activities_status`
--
ALTER TABLE `tb_activities_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_categories`
--
ALTER TABLE `tb_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_depart`
--
ALTER TABLE `tb_depart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_faculties`
--
ALTER TABLE `tb_faculties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_student`
--
ALTER TABLE `tb_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_title`
--
ALTER TABLE `tb_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
