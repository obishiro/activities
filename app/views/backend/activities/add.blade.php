@extends('template_backend.master_backend')
@section('content')
  <div class="row">
           

	  <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   	<h2><i class="fa fa-plus"></i> เพิ่มข้อมูลกิจกรรมนักศึกษา</h2>	
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   @if(Session::has('msg'))  
                     <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong><span class="label label-primary">บันทึกข้อมูลกิจกรรมนักศึกษา เสร็จเรียบร้อยแล้ว</span></strong>  เรื่อง  <a href="{{ URL::to('view/activities',array(Session::get('activities_id')))}}" class="alert-link"><i class="fa fa-link"></i> {{ Session::get('activities_name') }} คลิ๊กทีนี้ </a> 
                  </div>
                    @endif 
                     <form class="form-horizontal form-label-left" novalidate action="{{ URL::to('backend/add/activities')}}" method="POST">
                      
                     <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="website">หมวดหมู่กิจกรรม <span class="required">*</span>
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                           <select name="faculties_id" id="faculty" required="required" class="form-control col-md-7 col-xs-12">
                            <option value="">- เลือก -</option>
                            @foreach($category as $cat =>$c)
                            <option value="{{ $c->id}}">{{ $c->categories_name }}</option>
                           @endforeach
                          </select>
                        </div>
                      </div>
                       
                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="student_name">ชื่อกิจกรรม <span class="required">*</span>
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                          <input id="student_name" class="form-control col-md-7 col-xs-12"  name="activities_name"  required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="activities_date">วันที่จัดกิจกรรม <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                       
                          <input type="text" required="required" name="activities_date" class="form-control col-md-7 col-xs-12" value="<?php echo date('d-m-Y');?>" id="dateInput">
                                
                                 
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="activities_date">เวลาที่จัดกิจกรรม <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                       
                          <input type="text" required="required" name="activities_time" class="form-control col-md-7 col-xs-12" value="" placeholder="07:00">
                                
                                 
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">ปีการศึกษา<span class="required">*</span>
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                           <select name="activities_year" id="" required="required" class="form-control col-md-7 col-xs-12">
                            <option value="">- เลือก -</option>
                            {{ Helpers::GetYearTerm()}}
                           
                          </select>
                        </div>
                      </div>
              
                      
                      
                      
                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telephone">เทอมที่ <span class="required">*</span>
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                          <select name="activities_term" id="" required="required" class="form-control col-md-7 col-xs-12">
                            <option value="">- เลือก -</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                           </select>
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telephone">ชั้นปีที่เข้าร่วม <span class="required">*</span>
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                           <input type="checkbox" value="9" name="activities_for[]" class="check_all"> ทุกชั้นปี
                          &nbsp; <input type="checkbox" value="1" name="activities_for[]" class="check_item"> ชั้นปี 1
                          &nbsp; <input type="checkbox" value="2" name="activities_for[]" class="check_item"> ชั้นปี 2
                          &nbsp; <input type="checkbox" value="3" name="activities_for[]" class="check_item"> ชั้นปี 3
                          &nbsp; <input type="checkbox" value="4" name="activities_for[]" class="check_item"> ชั้นปี 4
                          &nbsp; <input type="checkbox" value="5" name="activities_for[]" class="check_item"> ชั้นปี 5
                        </div>
                      </div>

                     
                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telephone">รายละเอียดกิจกรรม <span class="required">*</span>
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                         
                           <textarea id="textarea" required="required" name="activities_detail" class="form-control col-md-7 col-xs-12"></textarea>
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="reset" class="btn btn-danger">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  
                    
                  </div>
                </div>
              </div>
	</div>
	</div>

@stop
 
@section('script')

         <script>
        var url = '<?php echo URL::to('/');?>';
        CKEDITOR.replace('textarea');
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required]', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

 

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
      $('#faculty').change(function(){

        var StrId=$('#faculty').val();

        $('#depart').load(url+'/backend/choose/depart/'+StrId,{
            ajax:true, test:$(this).val() });
  });
      $(":input").inputmask();
      $("#dateInput").datepicker({
        format: 'dd-mm-yyyy'
      });
      $('.check_all').click(function(){
        var checkbox = $(this);
        if (checkbox.is(":checked")) {
        $('.check_item').removeAttr("checked");
        $('.check_item').prop('disabled',true);
        }else{
          $('.check_item').prop('disabled',false);
        }


      });
    </script>
 
@stop