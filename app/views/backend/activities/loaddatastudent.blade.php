<table id="data-category" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th width="5%">ลำดับ</th>
                          <th width="10%">รหัส นศ.</th>
                          <th width="45%">ชื่อ_สกุล</th>
                          <th>ชั้นปี</th>
                          <th>ห้อง</th>
                          <th>เลือก</th>
                        
                        
                       
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($data as $datas => $c)
                        <tr>
                          <td>{{ $i }}</td>
                          <td>{{ $c->student_id}}</td>
                          <td>{{ $c->title_name}}{{ $c->student_name}} {{ $c->student_lastname}}</td>
                          <td>{{ $c->student_year}}</td>
                          <td>{{ $c->student_class}}</td>
                          <td><input type="checkbox" name="check[]"></td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
 
 <script src="{{ URL::to('js/jquery.min.js') }}"></script>
 <script src="{{ URL::to('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
  <script>
      
      $(document).ready(function() {
         $('#data-category').dataTable();
        });
    
        </script>
 