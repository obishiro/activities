@extends('template_backend.master_backend')
@section('content')
  <div class="row">
           

	  <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   	<h2><i class="fa fa-check-square-o" aria-hidden="true"></i>
 เช็คกิจกรรม : {{ $a->activities_name}}</h2>	
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   @if(Session::has('msg'))  
                     <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong><span class="label label-primary">บันทึกข้อมูลกิจกรรมนักศึกษา เสร็จเรียบร้อยแล้ว</span></strong>  เรื่อง  <a href="{{ URL::to('view/activities',array(Session::get('activities_id')))}}" class="alert-link"><i class="fa fa-link"></i> {{ Session::get('activities_name') }} คลิ๊กทีนี้ </a> 
                  </div>
                    @endif 
                     <form class="form-horizontal form-label-left" novalidate action="#" id="form_create">
               
                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telephone">คณะ <span class="required">*</span>
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                          <select name="faculties_id" id="faculty" required="required" class="form-control col-md-10 col-xs-12">
                            <option value="">- เลือก -</option>
                            @foreach($faculty as $fac =>$f)
                            <option value="{{ $f->id}}">{{ $f->faculties_name }}</option>
                           @endforeach
                           </select>
                        </div>
                      
                      </div>
                      <div class="item form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telephone">สาขา <span class="required">*</span>
                        </label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                          <select name="depart_id" id="depart" required="required" class="form-control col-md-10 col-xs-12">
                             
                           </select>
                        </div>
                      </div>
                        <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telephone">ชั้นปีที่ <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select name="student_year" id="student_year" required="required" class="form-control col-md-4 col-xs-12">
                            <option value=""></option>
                          <option value="1">ชั้นปี 1</option>
                          <option value="2">ชั้นปี 2</option>
                          <option value="3">ชั้นปี 3</option>
                           <option value="4">ชั้นปี 4</option>
                           <option value="5">ชั้นปี 5</option>
                          </select> 
                        </div>
                         <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telephone">ห้อง/หมู่เรียน <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <select name="student_class" id="student_class" required="required" class="form-control col-md-4 col-xs-12">
                            <option value="">- เลือก -</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                           
                          </select>
                        </div>
                        
                      </div>
                      <div class="item form-group">
                        <div class="col-md-2"></div>
                         <div class="col-md-10 ">
                          
                          <button id="send" type="button" class="btn btn-success btn-block">
                            <i class="fa fa-refresh" aria-hidden="true"></i> 
                            ดึงข้อมูลนักศึกษา</button>
                        </div>
                      </div>

                    
                      
                    </form>
                  
                    
                  </div>
                </div>
              </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-user" aria-hidden="true"></i>
                    ข้อมูลนักศึกษา</h2> 
                    <input type="text" placeholder="กรอกรหัสนักศึกษา" class="form-control">
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div id="data_student">
                      <h2 class="text-danger"><i class="fa fa-warning"></i> กรุณาเลือกข้อมูลนักศึกษาที่ต้องการ!</h2>
                    </div>
                  </div>
                  </div>
                  </div>
                 

	</div>
	</div>

@stop
 
@section('script')

         <script>
        var url = '<?php echo URL::to('/');?>';
          $('form')
        .on('blur', 'input[required]', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

 

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
 
      $('#faculty').change(function(){

        var StrId=$('#faculty').val();

        $('#depart').load(url+'/backend/choose/depart/'+StrId,{
            ajax:true, test:$(this).val() });
      });
      $('#send').click(function(){
        var faculty = $('#faculty').val();
        var depart = $('#depart').val();
        var student_year = $('#student_year').val();
        var student_class = $('#student_class').val();
        if(faculty=="" || depart=="" || student_class=="" || student_year=="")
        {
          alert('กรุณาเลือกข้อมูลให้ครบทุกช่องด้วย!');
        }else{
            $.ajax({
                url:url+"/backend/loaddatastudent",
                cache: false,
                  type:"POST",
                  data:$("#form_create").serialize(),
                     success:function(res){
                      if(res){
                          $('#data_student').html(res);

                                  
                      }
                  },
                          error:function(err){
                           alert('Error');
                          }                
                    });
        }
      });
 
    </script>
 
@stop