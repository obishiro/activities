@extends('template_backend.master_backend')
@section('content')
  <div class="row">
           

	  <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   	<h2>ข้อมูลกิจกรรมนักศึกษา</h2>	
                    <ul class="nav navbar-right panel_toolbox">
                       <li>
                         <button class="btn btn-success" onclick="location.href='{{ URL::to('backend/add/activities')}}' "><i class="fa fa-plus"></i> เพิ่มข้อมูล</button>
                       </li>
                      
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                  
                    <table id="data-activities" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th width="5%">ลำดับ</th>
                          <th>วันที่</th>
                          <th width="35%">ชื่อกิจกรรม</th>
                          <th>ปีการศึกษา</th>
                          <th>เทอม</th>
                          <th width="10%">ชั้นปี</th>
                          <th width="30%">หมวดหมู่กิจกรรม</th>
                         
                          <th>เครื่องมือ</th>
                       
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($data as $datas => $c)
                        <tr>
                          <td>{{ $i }}</td>
                          <td>{{ Helpers::ConvertDateToDisplay($c->activities_date)}} เวลา {{ $c->activities_time}}</td>
                          <td><a href="{{ URL::to('view/activities',array($c->id))}}">{{ $c->activities_name}}</a></td>
                          <td>{{ $c->activities_year}}</td>
                          <td>{{ $c->activities_term}}</td>
                          <td>{{ Helpers::ListActivitiesForDisplay($c->activities_for)}}</td>
                          <td>{{ $c->categories_name}}</td>
                      
                          <td width="13%">
                            <div class="btn-group">
                            <button type="button" class="btn btn-primary">การจัดการ</button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{ URL::to('backend/check/activities',array($c->id))}}"    ><span class="text-success"><i class="fa fa-check-square-o" aria-hidden="true"></i>
 เช็คการเข้าร่วมกิจกรรม</a></span></li>
   
                              <li><a href="{{ URL::to('backend/edit/activities',array($c->id))}}"    ><span class="text-warning"><i class="fa fa-pencil"></i> แก้ไข</a></span></li>
                                
                              <li><a href="{{ URL::to('backend/del/activities',array($c->id))}}" onclick="javascript:return confirm('ต้องการลบจริงหรือไม่?')" ><span class="text-danger"><i class="fa fa-trash" ></i> ลบ</span></a></li>
                             
                            </ul>
                          </div>
                          
                          

                          

                         
                          </td>
                          
                        </tr>
                       
                        <?php $i++; ?>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
	</div>
	</div>

@stop
 
@section('script')
  <script>
      
      $(document).ready(function() {
      	 $('#data-activities').dataTable();
      	});
    
      	</script>
         <script>
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required]', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

 

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
    </script>
 
@stop