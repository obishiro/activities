 <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{URL::to('backend/dashboard')}}" class="site_title"><i class="fa fa-trophy"></i> <span>ระบบกิจกรรม นศ.</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
 
              <div class="profile_pic">
               
              </div>
              <div class="profile_info">
               {{--  <span>Welcome,</span>
                <h2>John Doe</h2> --}}
 
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
 
                <ul class="nav side-menu active">
                  <li><a><i class="fa fa-home"></i> ตั้งค่าระบบ <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" >
                      <li><a href="{{ URL::to('backend/category')}}">ตั้งค่าหมวดหมู่กิจกรรม</a></li>
                      <li><a href="{{ URL::to('backend/faculty')}}">ตั้งค่าหมวดหมู่คณะ</a></li>
                      <li><a href="{{ URL::to('backend/depart')}}">ตังค่าสาขาวิชา</a></li>
                      <li><a href="{{ URL::to('backend/title')}}">ตังค่าคำนำหน้า</a></li>
                       
                    </ul>
                  </li>
                  <li><a><i class="fa fa-trophy"></i> ข้อมูลกิจกรรม <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ URL::to('backend/activities')}}">ข้อมูลกิจกรรม</a></li>
                       
                    </ul>
                  </li>
                  <li><a><i class="fa fa-user"></i> ข้อมูลนักศึกษา <span class="fa fa-chevron-down"></span></a>
                    <ul  class="nav child_menu">
                      <li><a href="{{ URL::to('backend/add/student')}}"><i class="fa fa-plus"></i> เพิ่มข้อมูลนักศึกษา</a></li>
                      @foreach($datamenu as $menu =>$mm)
                       <li><a href="#">{{ $mm->faculties_name}}<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li ><a href=""><i class="fa fa-eye"></i> ดูภาพรวมทั้งคณะ</a></li>
                              <?php $datadepart = Depart::where('faculties_id',$mm->id)->orderBy('depart_name','asc')->get(); ?>
                              @foreach($datadepart as $depart=>$d)
                            <li ><a href="{{ URL::to('backend/student',array($d->id))}}">{{ $d->depart_name}}</a>
                            </li>
                            @endforeach
                           
                           
                          </ul>
                        </li>
                         @endforeach
                       
                    </ul>
                  </li>
                   
                  <li><a href="{{ URL::to('backend/logout')}}" onclick="javascript:return confirm('ต้องการออกจากระบบ จริงหรือไม่?')"><i class="fa fa-power-off"></i> ออกจากระบบ </a>
                </ul>
              </div>
              

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
           
            <!-- /menu footer buttons -->
          </div>
        </div>