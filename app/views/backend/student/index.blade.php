@extends('template_backend.master_backend')
@section('content')
  <div class="row">
           

	  <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   	<h2>ข้อมูลนักศึกษา {{ $d->depart_name}}  {{ $d->faculties_name}}</h2>	
                    <ul class="nav navbar-right panel_toolbox">
                       <li>
                         <button  class="btn btn-success" onclick="location.href='{{ URL::to('backend/add/student')}}'" ><i class="fa fa-plus"></i> เพิ่มข้อมูล</button>
                       </li>
                  
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                  
                    <table id="data-category" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th width="5%">ลำดับ</th>
                          <th width="10%">รหัส นศ.</th>
                          <th width="45%">ชื่อ_สกุล</th>
                          <th>ชั้นปี</th>
                          <th>ห้อง</th>
                        
                          <th>เครื่องมือ</th>
                       
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($data as $datas => $c)
                        <tr>
                          <td>{{ $i }}</td>
                          <td>{{ $c->student_id}}</td>
                          <td>{{ $c->title_name}}{{ $c->student_name}} {{ $c->student_lastname}}</td>
                          <td>{{ $c->student_year}}</td>
                          <td>{{ $c->student_class}}</td>
                          <td>
                          <a href="{{ URL::to('backend/edit/student',array($c->id))}}"  class="btn btn-warning" ><i class="fa fa-pencil"></i> แก้ไข</a>
                          

                          <a href="{{ URL::to('backend/del/student',array($c->id,$c->student_id,$c->depart_id))}}" onclick="javascript:return confirm('ต้องการลบจริงหรือไม่?')" class="btn btn-danger"><i class="fa fa-trash"></i> ลบ</a>

                         
                          </td>
                          
                        </tr>
                       <div class="modal fade" id="frm-{{$i}}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-pencil"></i> แก้ไขข้อมูล</h4>
                        </div>
                        <div class="modal-body">
                           <form class="form-horizontal form-label-left" novalidate method="post" action="{{ URL::to('backend/edit/depart')}}">

                    
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ชื่อสาขาวิชา <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" class="form-control col-md-7 col-xs-12" value="{{ $c->depart_name}}"  name="txt-name" placeholder="กรอกข้อมูล" required="required" type="text">
                        </div>
                      </div>
                      <br><br>
                   
                    
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                      <br><br>
                      <input type="hidden" name="id" value="{{ $c->id}}">
                    </form>
                         </div>
                       

                      </div>
                    </div>
                  </div>
                        <?php $i++; ?>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
	</div>
	</div>

@stop
 
@section('script')
  <script>
      
      $(document).ready(function() {
      	 $('#data-category').dataTable();
      	});
    
      	</script>
         <script>
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required]', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

 

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
    </script>
 
@stop