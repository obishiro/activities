@extends('template_backend.master_backend')
@section('content')
  <div class="row">
           

	  <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   	<h2><i class="fa fa-plus"></i> เพิ่มข้อมูลนักศึกษา</h2>	
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   @if(Session::has('msg'))  
                     <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong><span class="label label-primary">บันทึกข้อมูลเสร็จเรียบร้อยแล้ว</span> 
                  </div>
                    @endif 
                     <form class="form-horizontal form-label-left" novalidate action="{{ URL::to('backend/add/student')}}" method="POST">
                      
                     
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">รหัสนักศึกษา <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="name" data-inputmask="'mask': '99999999999'" class="form-control col-md-7 col-xs-12" data-validate-length-range="11"   name="student_id"  required="required" type="text">
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">คำนำหน้า<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <select name="student_title" id="" required="required" class="form-control col-md-7 col-xs-12">
                            <option value="">- เลือก -</option>
                             @foreach($title as $tit =>$t)
                            <option value="{{ $t->id}}">{{ $t->title_name }}</option>
                           @endforeach
                           
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="student_name">ชื่อ <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="student_name" class="form-control col-md-7 col-xs-12"  name="student_name"  required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="student_lastname">นามสกุล <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="student_lastname" name="student_lastname" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">เพศ<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <select name="gender" id="" required="required" class="form-control col-md-7 col-xs-12">
                            <option value="">- เลือก -</option>
                            <option value="1">เพศชาย</option>
                            <option value="2">เพศหญิง</option>
                           
                          </select>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="website">คณะ <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <select name="faculties_id" id="faculty" required="required" class="form-control col-md-7 col-xs-12">
                            <option value="">- เลือก -</option>
                            @foreach($faculty as $fac =>$f)
                            <option value="{{ $f->id}}">{{ $f->faculties_name }}</option>
                           @endforeach
                          </select>
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">สาขาวิชา <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="depart_id" id="depart" required="required" class="form-control col-md-7 col-xs-12">
                            
                            
                           
                          </select>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">ชั้นปี <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="student_year" id="" required="required" class="form-control col-md-7 col-xs-12">
                            <option value="">- เลือก -</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                           
                          </select>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">ห้อง/หมู่เรียน <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="student_class" id="" required="required" class="form-control col-md-7 col-xs-12">
                            <option value="">- เลือก -</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                           
                          </select>
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="reset" class="btn btn-danger">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </form>
                  
                    
                  </div>
                </div>
              </div>
	</div>
	</div>

@stop
 
@section('script')
 
         <script>
        var url = '<?php echo URL::to('/');?>';
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required]', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

 

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
      $('#faculty').change(function(){

        var StrId=$('#faculty').val();

        $('#depart').load(url+'/backend/choose/depart/'+StrId,{
            ajax:true, test:$(this).val() });
  });
      $(":input").inputmask();
    </script>
 
@stop