<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ระบบกิจกรรมนักศึกษา </title>

    <!-- Bootstrap -->
    <link href="{{ URL::to('vendors/bootstrap/dist/css/bootstrap.min.css') }} " rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ URL::to('vendors/font-awesome/css/font-awesome.min.css') }} " rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ URL::to('vendors/iCheck/skins/flat/green.css') }} " rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{ URL::to('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }} " rel="stylesheet">
    <!-- jVectorMap -->
    

    <!-- Custom Theme Style -->
    <link href="{{ URL::to('build/css/custom.min.css') }} " rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       <?php
       $datamenu = Faculties::orderBy('faculties_name','asc')->get();
       ?> 
       @include('backend.menu',array('datamenu'=>$datamenu))
       @include('backend.top')
      

        <!-- page content -->
        <div class="right_col" role="main">
          
          @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by  
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ URL::to('js/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ URL::to('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ URL::to('vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ URL::to('vendors/nprogress/nprogress.js') }}"></script>
    <!-- Chart.js -->
 
    <!-- gauge.js -->
    <script src="{{ URL::to('vendors/bernii/gauge.js/dist/gauge.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ URL::to('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ URL::to('vendors/iCheck/icheck.min.js') }}"></script>
    <!-- Skycons -->
    <script src="{{ URL::to('vendors/skycons/skycons.js') }}"></script>
 
 
     
    
    <script src="{{ URL::to('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    
        <!-- NProgress -->
    <script src="{{ URL::to('vendors/nprogress/nprogress.js') }} "></script>
    <!-- validator -->
    <script src="{{ URL::to('vendors/validator/validator.min.js') }} "></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ URL::to('build/js/custom.js') }} "></script>
     <script src="{{URL::to('vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }} "></script>
       <script src="{{ asset('vendors/ckeditor/ckeditor.js') }}"></script>
       <script src="{{ asset('vendors/datepicker/bootstrap-datepicker.js') }}"></script>
      
    
    <script>
      $(document).ready(function() {
        var icons = new Skycons({
            "color": "#73879C"
          }),
          list = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
          ],
          i;

        for (i = list.length; i--;)
          icons.set(list[i], list[i]);

        icons.play();
      });
    </script>
    <!-- /Skycons -->

   
    @yield('script')
    <!-- /gauge.js -->
  </body>
</html>