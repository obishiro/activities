@extends('master_frontend')
@section('content')
<div class="row">
 <ol class="breadcrumb">
  <li><a href="{{ URL::to('/')}}">หน้าหลัก</a></li>
  
  <li class="active">กิจกรรมที่เข้าร่วมแล้ว</li>
</ol>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title text-primary"><i class="fa fa-check-circle"></i> กิจกรรมที่เข้าร่วมแล้ว</h3>
  </div>
  <div class="panel-body">
  <table id="data-activities" class="table table-striped table-bordered">
    <thead>
      <th width="5%">ลำดับ</th>
      <th>วันที่</th>
    <th width="35%">ชื่อกิจกรรม</th>
    <th>ปีการศึกษา</th>
    <th>เทอม</th>
    <th>สถานะ</th>
    
    </thead>
    <tbody>
       @foreach($data as $datas => $c)
                        <tr>
                          <td align="center">{{ $i }}</td>
                          <td  align="center">{{ Helpers::ConvertDateToDisplay($c->activities_date)}}</td>
                          <td><a href="{{ URL::to('view/activities',array($c->id))}}">{{ $c->activities_name}}</a></td>
                          <td align="center">{{ $c->activities_year}}</td>
                          <td align="center">{{ $c->activities_term}}</td>
                          <td align="center"><label for="" class="label label-success">เข้าร่วมแล้ว</label></td>
                        </tr>
                       
                        <?php $i++; ?>
                        @endforeach
    </tbody>
  </table>
  </div>
  <div class="panel-footer"></div>
</div>

</div>

       
@stop
@section('script')
 <script src="{{ URL::to('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::to('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
  <script>
      
      $(document).ready(function() {
         $('#data-activities').dataTable();
        });
    
  </script>
  @stop
