@extends('master_frontend')
@section('content')
<div class="thumbnail">
                    <img class="img-rounded img-responsive" src="{{URL::to('images/thumb.png')}}" alt="">
                    
                     
                </div>

               <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bell"></i> รายการกิจกรรมล่าสุด</h3>
              </div>
              <div class="panel-body">
               <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                
                <div class="x_content">
                  <ul class="list-unstyled timeline">
                     @foreach($data as $datas=>$d)  
                    <li>
                      <div class="block">
                        <div class="tags">
                          <a href="{{ URL::to('view/activities',array($d->id))}}"  class="tag">
                            <span>{{  Helpers::ConvertDateToDisplay($d->activities_date)}}</span>
                          </a>
                        </div>
                        <div class="block_content">
                          <h2 class="title">
                                          <a href="{{ URL::to('view/activities',array($d->id))}}">{{ $d->activities_name}}</a>
                                      </h2>
                          <div class="byline">
                            <span class="label label-danger">เวลา  08.00 น.</span>
                          </div>
                          <span style="height:10px"></span>
                          <p class="excerpt">{{ $d->activities_detail}} <a href="{{ URL::to('view/activities',array($d->id))}}">อ่านเพิ่มเติม</a>
                          </p>
                        </div>
                      </div>
                    </li>
                    @endforeach  
                   
                  </ul>
                  <?php echo $data->links(); ?>
                </div>
              </div>
            </div>
              </div>
            </div>
@stop