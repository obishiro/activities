@extends('master_frontend')
@section('content')
<div class="row">
 <ol class="breadcrumb">
  <li><a href="{{ URL::to('/')}}">หน้าหลัก</a></li>
  <li><a href="{{ URL::to('view/categories',array($d->categories_id))}}">{{ $d->categories_name}}</a></li>
  <li class="active">{{$d->activities_name}}</li>
</ol>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title text-primary"><i class="fa fa-bell"></i> {{ $d->activities_name}}</h3>
  </div>
  <div class="panel-body">
    ประจำปีการศึกษา 
    <span class="label label-success">{{ $d->activities_year}}</span>
    เทอมที่
<span class="label label-primary">{{ $d->activities_term}}</span>
  ชั้นปีที่เข้าร่วมกิจกรรม
 <span class="label label-danger">{{ Helpers::ListActivitiesForDisplay($d->activities_for)}}</span>
 <hr>
   {{ $d->activities_detail}}
  </div>
  <div class="panel-footer"><i class="fa fa-pencil"></i> สร้างเมื่อ <i class="fa fa-user"></i> โดย <i class="fa fa-eye"></i> อ่าน 1 ครั้ง</div>
</div>

</div>

       
@stop