<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('view/{type}/{id}', 'HomeController@getView')->where('id', '[0-9]+');
Route::get('activities','HomeController@getActivities');
Route::get('absence','HomeController@getAbsence');
Route::get('unabsence','HomeController@getUnabsence');
Route::get('login','BackendController@getLogin');
Route::post('login','BackendController@postLogin');
Route::group(array('before' => 'auth'), function() {
  Route::controller('backend','BackendController');
 
});
