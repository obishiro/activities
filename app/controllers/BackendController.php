<?php

class BackendController extends BaseController {
 
	public function getLogin()
	{
		return View::make('backend.login');
	}
	public function postLogin()
	{
		  
    	 	$rules = array(
			'username'	=> 'required',
			'password'	=> 'required'

								);
				$validator = Validator::make(Input::all(), $rules);

    		if ($validator->fails())
    			{
       		 		return Redirect::to('login')->withErrors($validator)->withInput();
     			}else{
 
		       if(Auth::attempt(array('username'=>Input::get('username'),'password'=>Input::get('password'),'user_status'=>'1'))) {
		          
		       			switch(Auth::user()->user_type):
		       				case '2':
		       					return Redirect::to('/');
		       				break;
		       				case '1':
		       					return Redirect::to('backend/dashboard');
		       				break;
		       			endswitch;
		       
		            }else{
				        return Redirect::to('login')
				        ->with('error_login_incorrect',Lang::get('msg.error_login_incorrect',array(),'th'));
				      }
				  }
	}
	public function getDashboard()
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
			//return Auth::user()->id;
		return View::make('backend.dashboard');
		}else{
			return Redirect::to('login');
		}
	}
	public function getCategory()
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		$data = Categories::orderBy('id','desc')->get();
		return View::make('backend.category.index')->with(array('data'=>$data,'i'=>1));
		}else{
			return Redirect::to('login');
		}
	}
		public function getTitle()
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		$data = Title::orderBy('id','desc')->get();
		return View::make('backend.title.index')->with(array('data'=>$data,'i'=>1));
		}else{
			return Redirect::to('login');
		}
	}
		public function getFaculty()
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		$data = Faculties::orderBy('id','desc')->get();
		return View::make('backend.faculty.index')->with(array('data'=>$data,'i'=>1));
		}else{
			return Redirect::to('login');
		}
	}
		public function getDepart()
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		$data = Depart::select('tb_depart.id','tb_depart.faculties_id','tb_depart.depart_name','tb_faculties.faculties_name','tb_depart.created_at','tb_depart.updated_at')->join('tb_faculties','tb_depart.faculties_id','=','tb_faculties.id')
		->orderBy('tb_depart.id','desc')->get();
		$faculties = Faculties::orderBy('faculties_name','asc')->get();
		return View::make('backend.depart.index')->with(array('data'=>$data,'i'=>1,'faculties'=>$faculties));
		}else{
			return Redirect::to('login');
		}
	}
		public function getStudent($id)
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		$data = Student::select('tb_student.*','tb_depart.depart_name','tb_title.title_name')
		->join('tb_depart','tb_depart.id','=','tb_student.depart_id')
		->join('tb_title','tb_title.id','=','tb_student.title')

		->where('tb_student.depart_id',$id)
		->orderBy('tb_student.student_id','asc')->get();
		$depart=Depart::select('tb_depart.depart_name','tb_faculties.faculties_name')
		->join('tb_faculties','tb_faculties.id','=','tb_depart.faculties_id')
		->where('tb_depart.id','=',$id)
		->first();
		return View::make('backend.student.index')->with(array('data'=>$data,'i'=>1,'d'=>$depart));
		}else{
			return Redirect::to('login');
		}
	}
	public function getActivities()
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
			 
			$data = Activities::select('tb_activities.*','tb_categories.categories_name')
			->join('tb_categories','tb_activities.categories_id','=','tb_categories.id')
			->orderBy('tb_activities.id','desc')
			->get();
			return View::make('backend.activities.index')->with(array('data'=>$data,'i'=>1));
		}else{
			return Redirect::to('login');
		}
	}
	public function getAdd($type) 
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		switch ($type) {
			case 'student':
				$Faculties = Faculties::orderBy('faculties_name','asc')->get();
				$Title = Title::orderBy('id','asc')->get();
				return View::make('backend.student.add')
						->with(array(
							'faculty' => $Faculties,
							'title'  => $Title
							
							));
				break;
			case 'activities':
				$category = Categories::orderBy('categories_name','asc')->get();
				return View::make('backend.activities.add')
						->with(array(
							'category' => $category
							
							));
			break;		
			
			 
		}
		}else{
			return Redirect::to('login');
		}
	}
	public function postAdd($type)
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		switch ($type) {
			case 'category':
			 	$c = new Categories;
			 	$c->categories_name = Input::get('txt-name');
			 	$c->created_at = date('Y-m-d H:i:s');
			 	$c->updated_at = date('Y-m-d H:i:s');
			 	$c->save();
			 	return Redirect::to('backend/category');
			break;
			case 'title':
			 	$c = new Title;
			 	$c->title_name = Input::get('txt-name');
			 	$c->created_at = date('Y-m-d H:i:s');
			 	$c->updated_at = date('Y-m-d H:i:s');
			 	$c->save();
			 	return Redirect::to('backend/title');
			break;
			case 'faculty':
			 	$c = new Faculties;
			 	$c->faculties_name = Input::get('txt-name');
			 	$c->created_at = date('Y-m-d H:i:s');
			 	$c->updated_at = date('Y-m-d H:i:s');
			 	$c->save();
			 	return Redirect::to('backend/faculty');
			break;
			case 'depart':
			 	$c = new Depart;
			 	$c->depart_name = Input::get('txt-name');
			 	$c->faculties_id = Input::get('faculties');
			 	$c->created_at = date('Y-m-d H:i:s');
			 	$c->updated_at = date('Y-m-d H:i:s');
			 	$c->save();
			 	return Redirect::to('backend/depart');
			break;
			case 'student':
				$s = new Student;
				$s->student_id 			= Input::get('student_id');
				$s->title 				= Input::get('student_title');
				$s->student_name 		= Input::get('student_name');
				$s->student_lastname	= Input::get('student_lastname');
				$s->gender 				= Input::get('gender');
				$s->faculties_id		= Input::get('faculties_id');
				$s->depart_id 			= Input::get('depart_id');
				$s->student_year 		= Input::get('student_year');
				$s->student_class 		= Input::get('student_class');
				$s->created_at = date('Y-m-d H:i:s');
			 	$s->updated_at = date('Y-m-d H:i:s');
				$s->save();
				$u = new User;
				$u->username = Input::get('student_id');
				$u->password = Hash::make(Input::get('student_id'));
				$u->user_status ='1';
				$u->user_type = '2';
				$u->created_at = date('Y-m-d H:i:s');
			 	$u->updated_at = date('Y-m-d H:i:s');
			 	$u->save();

				$student_id = Input::get('student_id');
				return Redirect::to('backend/add/student')->with(array(
					'msg' => '1',
					'student_id' =>$student_id
					));
			break;
			case 'activities':
				 $date = Helpers::ConvertDateToSql(Input::get('activities_date'));
				$activities_for = Helpers::ListActivitiesFor(Input::get('activities_for'));
				 
				$a = new Activities;
				$a->categories_id = Input::get('faculties_id');
				$a->activities_name = Input::get('activities_name');
				$a->activities_date = $date;
				$a->activities_time = Input::get('activities_name');
				$a->activities_detail = Input::get('activities_detail');
				$a->activities_year = Input::get('activities_year');
				$a->activities_term = Input::get('activities_term');
				$a->activities_for = $activities_for;
				$a->activities_by = Auth::user()->id;
				$a->created_at = date('Y-m-d H:i:s');
			 	$a->updated_at = date('Y-m-d H:i:s');
				$a->save();
				$v = Activities::orderBy('id','desc')->take(1)->skip(0)->first();
				return Redirect::to('backend/add/activities')->with(array(
					'msg' => '1',
					'activities_name'=>$v->activities_name,
					'activities_id'=>$v->id
				 
					));

			break;
			
			 
		}
		}else{
			return Redirect::to('login');
		}
	}
	public function getEdit($type,$id)
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
			switch($type)
			{
				case 'activities':
					$a = Activities::find($id);
					$category = Categories::orderBy('categories_name','asc')->get();
				return View::make('backend.activities.edit')
						->with(array(
							'category' => $category,
							'a'		=>$a
							
							));
				break;
				case 'student':
					$Faculties = Faculties::orderBy('faculties_name','asc')->get();
				$Title = Title::orderBy('id','asc')->get();
				return View::make('backend.student.edit')
						->with(array(
							'faculty' => $Faculties,
							'title'  => $Title
							
							));
				break;
			}
		}else{
			return Redirect::to('login');
		}
	}
	public function postEdit($type)
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		switch ($type) {
			case 'category':
				$id = Input::get('id');
			 	$c = Categories::find($id);
			 	$c->categories_name = Input::get('txt-name');
			 	$c->created_at = date('Y-m-d H:i:s');
			 	$c->updated_at = date('Y-m-d H:i:s');
			 	$c->save();
			 	return Redirect::to('backend/category');
			break;
			case 'title':
				$id = Input::get('id');
			 	$c = Title::find($id);
			 	$c->title_name = Input::get('txt-name');
			 	$c->created_at = date('Y-m-d H:i:s');
			 	$c->updated_at = date('Y-m-d H:i:s');
			 	$c->save();
			 	return Redirect::to('backend/title');
			break;
			case 'faculty':
				$id = Input::get('id');
			 	$c = Faculties::find($id);
			 	$c->faculties_name= Input::get('txt-name');
			 	$c->created_at = date('Y-m-d H:i:s');
			 	$c->updated_at = date('Y-m-d H:i:s');
			 	$c->save();
			 	return Redirect::to('backend/faculty');
			break;
			case 'depart':
				$id = Input::get('id');
			 	$c = Depart::find($id);
			 	$c->depart_name= Input::get('txt-name');
			 	$c->faculties_id = Input::get('faculties');
			 	$c->created_at = date('Y-m-d H:i:s');
			 	$c->updated_at = date('Y-m-d H:i:s');
			 	$c->save();
			 	return Redirect::to('backend/depart');
			break;
			case 'activities':
				$date = Helpers::ConvertDateToSql(Input::get('activities_date'));
				$activities_for = Helpers::ListActivitiesFor(Input::get('activities_for'));
				$id = Input::get('id');
				$a = Activities::find($id);
				$a->categories_id = Input::get('faculties_id');
				$a->activities_name = Input::get('activities_name');
				$a->activities_date = $date;
				$a->activities_time = Input::get('activities_time');
				$a->activities_detail = Input::get('activities_detail');
				$a->activities_year = Input::get('activities_year');
				$a->activities_term = Input::get('activities_term');
				$a->activities_for = $activities_for;
				$a->activities_by = Auth::user()->id;
				//$a->created_at = date('Y-m-d H:i:s');
			 	$a->updated_at = date('Y-m-d H:i:s');
				$a->save();
				$v = Activities::find($id);
				return Redirect::to('backend/edit/activities/'.$id)->with(array(
					'msg' => '1',
					'activities_name'=>$v->activities_name,
					'activities_id'=>$v->id
				 
					));
			break;
			
			 
		}
		}else{
			return Redirect::to('login');
		}
	}
	public function getDel($type,$id,$pid=null,$depart=null)
	{
		if(Auth::check() && Auth::user()->user_type=="1")
		{
		switch ($type) {
			case 'category':
			 	$c = Categories::find($id);
			 	$c->delete();
			 	return Redirect::to('backend/category');
			break;
			case 'title':
			 	$c = Title::find($id);
			 	$c->delete();
			 	return Redirect::to('backend/title');
			break;
			case 'faculty':
			 	$c = Faculties::find($id);
			 	$c->delete();
			 	return Redirect::to('backend/faculty');
			break;
			case 'depart':
			 	$c = Depart::find($id);
			 	$c->delete();
			 	return Redirect::to('backend/depart');
			break;
			case 'activities':
			 	$c = Activities::find($id);
			 	$c->delete();
			 	return Redirect::to('backend/activities');
			break;
			case 'student':
				$s = Student::find($id);
				$s->delete();
				$sa = User::where('username',$pid)->delete();
			 	return Redirect::action('BackendController@getStudent',array($depart));

			break;
			 
		}
		}else{
			return Redirect::to('login');
		}
	}

	public function postChoose($type,$id)
	{
		switch ($type) {
			case 'depart':
				$faculty= Depart::where('faculties_id',$id)->orderBy('depart_name','asc')->get();
				return View::make('backend.student.choosedepart')
						->with(array(
							'faculty' => $faculty
					));
				break;
			
			 
		}
	}
	public function getCheck($type,$id)
	{
		switch($type) {
			case 'activities':
			$activities =Activities::find($id);
			$depart = Depart::orderBy('depart_name','asc')->get();
			$faculty = Faculties::orderBy('faculties_name','asc')->get();
			return View::make('backend.activities.check')
						->with(array(
							'faculty' => $faculty,
							'depart'  => $depart,
							'a'			=>$activities
					));
			break;
		}
	}
	public function postLoaddatastudent()
	{
		$data = Student::where(array(
			'faculties_id'=>Input::get('faculties_id'),
			'depart_id'		=>Input::get('depart_id'),
			'student_year' =>Input::get('student_year'),
			'student_class' =>Input::get('student_class')
			))->orderBy('student_id','asc')->get();
		return View::make('backend.activities.loaddatastudent')
		->with(array(
			'data'=>$data,
			'i'	  =>1	
			));
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('/');
	}
}