<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$title ='ระบบกิจกรรมนักศึกษา';
		
		if(Auth::check() && Auth::user()->user_type=="2")
		{

			$student = Student::where('student_id',Auth::user()->username)->first();

			$all_activities = Activities::where('activities_for','=','9')
			->orWhere('activities_for','like','%'.$student->student_year.'%')->count();
			 
			 
			$unabsence =Activitiesstatus::where('student_id','=',$student->student_id)->count();
		 	$absence = $all_activities - $unabsence;
			
			$data = Activities::select('tb_activities.*','tb_categories.categories_name')
			->where('tb_activities.activities_for','=','9')
			->orWhere('tb_activities.activities_for','like','%'.$student->student_year.'%')
			->join('tb_categories','tb_activities.categories_id','=','tb_categories.id')
			->orderBy('tb_activities.id','desc')
			->paginate(10);

		return View::make('frontend.index')->with(
			array(
				'data'=>$data,
				'title'=>$title,
				'all'=>number_format($all_activities),
				'unabsence' =>number_format($unabsence),
			 	'absence' =>number_format($absence)
				));
		}else{ 
		$data = Activities::select('tb_activities.*','tb_categories.categories_name')
			->join('tb_categories','tb_activities.categories_id','=','tb_categories.id')
			->orderBy('tb_activities.id','desc')->paginate(10);
		return View::make('frontend.index')->with(array('data'=>$data,'title'=>$title));
		}
	}
	public function getView($type,$id)
	{
		switch ($type) {
			case 'activities':
			if(Auth::check() && Auth::user()->user_type=="2")
		{

			$student = Student::where('student_id',Auth::user()->username)->first();

			$all_activities = Activities::where('activities_for','=','9')
			->orWhere('activities_for','like','%'.$student->student_year.'%')->count();
			 
			 
			$unabsence =Activitiesstatus::where('student_id','=',$student->student_id)->count();
		 	$absence = $all_activities - $unabsence;
		 	$data = Activities::select('tb_activities.*','tb_categories.categories_name')
		
			->join('tb_categories','tb_activities.categories_id','=','tb_categories.id')
			->where('tb_activities.id','=',$id)->first();
				return View::make('frontend.view')->with(
					array('d'=>$data,
						'title'=>$data->activities_name,
						'all'=>number_format($all_activities),
						'unabsence' =>number_format($unabsence),
					 	'absence' =>number_format($absence)

						));

		 }else{
				$data = Activities::select('tb_activities.*','tb_categories.categories_name')
		
			->join('tb_categories','tb_activities.categories_id','=','tb_categories.id')
			->where('tb_activities.id','=',$id)->first();
				return View::make('frontend.view')->with(array('d'=>$data,'title'=>$data->activities_name));
			}
				break;
			 
		}
	}
	public function getActivities()

	{
		if(Auth::check() && Auth::user()->user_type=="2")
		{

			$student = Student::where('student_id',Auth::user()->username)->first();

			$all_activities = Activities::where('activities_for','=','9')
			->orWhere('activities_for','like','%'.$student->student_year.'%')->count();
			$unabsence =Activitiesstatus::where('student_id','=',$student->student_id)->count();
		 	$absence = $all_activities - $unabsence;
		 	$data = Activities::select('tb_activities.*','tb_categories.categories_name')
			->where('tb_activities.activities_for','=','9')
			->orWhere('tb_activities.activities_for','like','%'.$student->student_year.'%')
			->join('tb_categories','tb_activities.categories_id','=','tb_categories.id')
			->orderBy('tb_activities.id','desc')->get();
			$data_absence = Activities::select('tb_activities.*','tb_activities_status.student_id','tb_student.student_id')
			->join('tb_activities_status','tb_activities_status.activities_id','=','tb_activities.id')
			->join('tb_student','tb_student.student_id','=','tb_activities_status.student_id')
			->orderBy('tb_activities.id','desc')->get();
		 	return View::make('frontend.activities')->with(
					array(
						'data'	=>$data,
						'data_absence'	=>$data_absence,
						'i'		=>1,
						'title'=>'กิจกรรมทั้งหมด',
						'all'=>number_format($all_activities),
						'unabsence' =>number_format($unabsence),
					 	'absence' =>number_format($absence)

						));
		 }
		
	}
	public function getAbsence()

	{
		if(Auth::check() && Auth::user()->user_type=="2")
		{

			$student = Student::where('student_id',Auth::user()->username)->first();

			$all_activities = Activities::where('activities_for','=','9')
			->orWhere('activities_for','like','%'.$student->student_year.'%')->count();
			$unabsence =Activitiesstatus::where('student_id','=',$student->student_id)->count();
		 	$absence = $all_activities - $unabsence;
		 	$data = DB::select('SELECT 
 tb_activities.*
FROM 
   tb_activities
   LEFT OUTER JOIN tb_activities_status ON tb_activities.id = tb_activities_status.activities_id
 
WHERE tb_activities_status.id IS NULL order by tb_activities.id asc');
		 	return View::make('frontend.absence')->with(
					array(
						'data'	=>$data,
						'i'		=>1,
						'title'=>'กิจกรรมที่ไม่ได้เข้าร่วม',
						'all'=>number_format($all_activities),
						'unabsence' =>number_format($unabsence),
					 	'absence' =>number_format($absence)

						));
		 }
		
	}

	public function getUnabsence()

	{
		if(Auth::check() && Auth::user()->user_type=="2")
		{

			$student = Student::where('student_id',Auth::user()->username)->first();

			$all_activities = Activities::where('activities_for','=','9')
			->orWhere('activities_for','like','%'.$student->student_year.'%')->count();
			$unabsence =Activitiesstatus::where('student_id','=',$student->student_id)->count();
		 	$absence = $all_activities - $unabsence;
		 	$data = Activities::select('tb_activities.*','tb_activities_status.student_id','tb_student.student_id')
			->join('tb_activities_status','tb_activities_status.activities_id','=','tb_activities.id')
			->join('tb_student','tb_student.student_id','=','tb_activities_status.student_id')
			->orderBy('tb_activities.id','desc')->get();
		 	return View::make('frontend.unabsence')->with(
					array(
						'data'	=>$data,
						'i'		=>1,
						'title'=>'กิจกรรมที่เข้าร่วมแล้ว',
						'all'=>number_format($all_activities),
						'unabsence' =>number_format($unabsence),
					 	'absence' =>number_format($absence)

						));
		 }
		
	}

}
