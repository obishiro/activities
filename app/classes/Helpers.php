<?php
class Helpers {
	 public static function ListClass($array,$number)
	 {
	  	$arr = str_split($array);
	  	if(in_array($number, $arr,true)){
	  		return 'checked="true"';
	  	}else{
	  		return "";
	  	}
	}

	public static function GetClass($array,$number)
	 {
	  	$arr = str_split($array);
	  	if(in_array($number, $arr,true)){
	  		return $number;
	  	}
	}
	 			 
	public static function GetYearTerm($year=null)
	{
		$y = (date('Y')+543)+1;
	 
		$beginyear = 2559;
		for($i=$beginyear;$i<=$y;$i++){
			if($year == $i)
			{
				$selected = "selected='true'";
			}else{
				$selected="";
			}
			echo "<option value=\"$i\" $selected> ".$i."</option>";
		}
	}
	public static function ConvertDateToSql($date)
	{

		$d = explode('-', $date);
		$day = $d['0'];
		$month = $d['1'];
		$year = $d['2'];
		$newdate = $year.'-'.$month.'-'.$day;
		return $newdate;
	}
		public static function ConvertDateToField($date)
	{

		$d = explode('-', $date);
		$day = $d['2'];
		$month = $d['1'];
		$year = $d['0'];
		$newdate = $day.'-'.$month.'-'.$year;
		return $newdate;
	}
	public	static function ConvertDateToDisplay($dateeng)
	{
		$date = strtotime($dateeng);
		$thai_month_arr=array(  
    "0"=>"",  
    "1"=>"ม.ค.",  
    "2"=>"ก.พ.",  
    "3"=>"มี.ค.",  
    "4"=>"เม.ย.",  
    "5"=>"พ.ค.",  
    "6"=>"มิ.ย.",   
    "7"=>"ก.ค.",  
    "8"=>"ส.ค.",  
    "9"=>"ก.ย.",  
    "10"=>"ต.ค.",  
    "11"=>"พ.ย.",  
    "12"=>"ธ.ค."                    
);  
	    
	    $thai_date_return= date("j",$date);  
	    $thai_date_return.=" ".$thai_month_arr[date("n",$date)];  
	    $thai_date_return.= (date("Yํ",$date)+543);  
	   // $thai_date_return.= "  ".date("H:i",$date)." น."; 
	    return $thai_date_return; 
	}
	public static function ListActivitiesFor($array)
	{
		if(count($array)==1){
			foreach($array as $arr =>$a) 
		{

			return $a;
		}
		}else{
	 			return implode('', $array);
			}
	}
	public static function ListActivitiesForDisplay($array)
	{
		if($array==9)
		{
			return 'ทุกชั้นปี';
		
		}else{
				$arr = str_split($array);
	 			return 'ชั้นปีที่ '.implode(',', $arr);
			}
	}
}